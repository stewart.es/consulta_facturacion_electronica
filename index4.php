<?php
include "db.php";
include "clases/facturacion.php";
include "clases/base_de_datos.php";
/*
F023-0009181
363.22
*/
$doc = new DOMDocument();
$doc->load("F023.xml");
$xpath = new DOMXPath($doc);
$xpath->registerNamespace("neogrid","http://www.neogrid.com");
$xpath->registerNamespace("principal","urn:oasis:names:specification:ubl:schema:xsd:Invoice-2");
$xpath->registerNamespace("cac","urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2");
$xpath->registerNamespace("cbc","urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2");
$xpath->registerNamespace("ccts","urn:un:unece:uncefact:documentation:2");
$xpath->registerNamespace("sac","urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1");
$xpath->registerNamespace("ext","urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2");

$consulta ='/neogrid:NeoGridFactura/neogrid:OriginalMessage/principal:Invoice/cbc:IssueDate';
$telefacturacion=new Telefacturacion();
$valor=$telefacturacion->tf_obtener_valor_xml($doc,$consulta,$xpath);
//echo "El valor es ".$valor."<br>";


$valor=$telefacturacion->tf_obtener_valor_xml($doc,'/neogrid:NeoGridFactura/neogrid:OriginalMessage/principal:Invoice/cbc:ID',$xpath);
$valor=$telefacturacion->tf_obtener_valor_xml($doc,'/neogrid:NeoGridFactura/neogrid:OriginalMessage/principal:Invoice/cbc:IssueDate',$xpath);
$valor=$telefacturacion->tf_obtener_valor_xml($doc,'/neogrid:NeoGridFactura/neogrid:OriginalMessage/principal:Invoice/cbc:InvoiceTypeCode',$xpath);
$valor=$telefacturacion->tf_obtener_valor_xml($doc,'/neogrid:NeoGridFactura/neogrid:OriginalMessage/principal:Invoice/cbc:DocumentCurrencyCode',$xpath);




$array = $xpath->evaluate('/neogrid:NeoGridFactura/neogrid:OriginalMessage/principal:Invoice/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sac:AdditionalInformation/sac:AdditionalMonetaryTotal');
$facturacion=new facturacion();
$res=$facturacion->obtener_valores($array);


$array = $xpath->evaluate('/neogrid:NeoGridFactura/neogrid:OriginalMessage/principal:Invoice/cac:TaxTotal');
$res=$facturacion->obtener_impuestos($array);


$array = $xpath->evaluate('/neogrid:NeoGridFactura/neogrid:OriginalMessage/principal:Invoice/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sac:AdditionalInformation/sac:AdditionalProperty');
$res=$facturacion->obtener_monto_en_letras($array);
$otros_datos=$facturacion->datos_factura($xpath);

$articulos=$facturacion->articulos($xpath);

$facturacion->mostrar_factura();
//print_r($facturacion->captura);
print_r($facturacion->monto);
print_r($facturacion->datos_factura);
print_r($facturacion->articulos);


function tf_obtener_valor_xml($documento,$consulta,$xpath){
	$result = $xpath->query($consulta, $documento);
	return $result->item(0)->nodeValue;

}
Class Telefacturacion{
/*
Telefacturación Del Perú S.A.C
Portinari 196 D 301 - San Borja - Lima - Perú
2232649 - 992233565
*/
public function tf_obtener_valor_xml($documento,$consulta,$xpath){
	$result = $xpath->query($consulta, $documento);
	return $result->item(0)->nodeValue;

}
}


//$xml = simplexml_load_file("F023.xml");
//$products = $xml->xpath("//NeoGridFactura/OriginalMessage/Invoice/ext:UBLExtensions");
//print_r($products);

//print $xmlDoc->saveXML();
?>