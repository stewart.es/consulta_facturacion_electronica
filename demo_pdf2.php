<?php
require_once '_x2dompdf/lib/html5lib/Parser.php';
require_once '_x2dompdf/lib/php-font-lib/src/FontLib/Autoloader.php';
require_once '_x2dompdf/lib/php-svg-lib/src/autoload.php';
require_once '_x2dompdf/src/Autoloader.php';
Dompdf\Autoloader::register();

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
// instantiate and use the dompdf class
$options = new Options();
$options->set('defaultFont', 'Helvetica');
$dompdf = new Dompdf($options);

$bootstrap='
.row {
    display: block;
    margin-right: -15px;
    margin-left: -15px;
}
.col-md-12 {
    
    flex: 0 0 100%;
    max-width: 100%;
        


}
.col-md-7 {
    
    flex: 0 0 58.333333%;
    max-width: 58.333333%;
float:left;
    position:relative
display:block;
}
.col-md-6 {
    
    flex: 0 0 50%;
    max-width: 50%;
    float:left;
    position:relative
display:block;
}
.col-md-5 {
    
    flex: 0 0 41.666667%;
    max-width: 41.666667%;
display:block;
}
.col-md-2 {
    
    flex: 0 0 16.666667%;
    max-width: 16.666667%;
        float:left;
    position:relative
}
.col-md-1 {
    
    flex: 0 0 8.333333%;
    max-width: 8.333333%;
    float:left;
    position:relative;
    display:block;
}
.col, .col-1, .col-10, .col-11, .col-12, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-auto, .col-lg, .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-auto, .col-md, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-auto, .col-sm, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-auto, .col-xl, .col-xl-1, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-auto {
    position: relative;
    width: 100%;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
    
}
.offset-md-2 {
    margin-left: 16.666667%;
}
.text-right {
    text-align: right !important;
}
';
$html ='
<style>
@font-face {
    font-family: "Orbitron";
    src: url("fuentes/orbitron-black-webfont.ttf") format("truetype");
    font-weight: bolder;
    font-style: normal;
}  
@font-face {
    font-family: "Orbitron";
    src: url("fuentes/orbitron-bold-webfont.ttf") format("truetype");
    font-weight: bold;
    font-style: normal;
} 
@font-face {
    font-family: "Orbitron";
    src: url("fuentes/orbitron-medium-webfont.ttf") format("truetype");
    font-weight: normal;
    font-style: normal;
}  
@font-face {
    font-family: "Orbitron";
    src: url("fuentes/orbitron-medium-webfont.ttf") format("truetype");
    font-weight: lighter;
    font-style: normal;
} 

@font-face {
    font-family: "Oxygen";
    src: url("fuentes/Oxygen-Regular.ttf") format("truetype");
    font-weight: normal;
    font-style: normal;
}  
'.$bootstrap.'
	body {
  /*min-height: 2000px;*/
  padding-top: 00px;
      text-shadow: 1px 1px 1px rgba(0,0,0,0.004);
}
.dropdown-menu {
  position: fixed !important;
  width:100%;
  height:auto;
  top: 95px !important;
  }


  .navbar-expand-md .navbar-nav > li > a {
    color: #fff;
    text-transform: uppercase;
    font-size: 14px;
    letter-spacing: .12em;
    margin-top: 30px;
    font-weight: 600;
}

.fondo_celeste{
  background-color: #00a8ff;
  background-image: url("imagenes/menu_fondo1.jpg");
  background-position: cover;
}
.submenu_general{
  padding: 60px 10px;

}

.dropdown-menu .title {
    font-size: 16px;
    font-weight: 500;
    margin-top: 15px;
    text-transform: uppercase;
    border-bottom: 1px solid #70a338;
    padding-bottom: 10px;
    color: #fff;
    font-family: "Orbitron";
}
.dropdown-menu p {
    color: #656565;
    
    
    font-style: normal;
    font-size: 1.0em;
   /* line-height: 2.0em;*/
    color: #fff;
}
.dropdown-menu .dropdown-menu a, .dropdown-menu a {
    color: #fff;
    font-weight: 500;
}
.dropdown-menu ul {
    list-style: none;
    padding-left: 0px;
}
.dropdown-menu li {
    color: #fff;
    font-weight: 500;
   /* margin: 10px 0;*/
}
.view-all {
    display: inline-block;
    font-size: 14px !important;
    font-weight: 600 !important;
    margin-top: 15px;
}

header {
    flex-direction: column;
}
header {
    width: 100%;
    text-align: center;
    overflow: hidden;
    position: relative;
    display: -webkit-box;
    display: -moz-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -ms-flex-line-pack: center;
    align-content: center;
}
header.banner .banner-text {
    -webkit-transform: translate(0%, -50%);
    -ms-transform: translate(0%, -50%);
    transform: translate(0%, -50%);
    top: 50%;
}
header.banner .banner-text {
    position: absolute;
    width: 90%;
    left: 5%;
    z-index: 30;
    align-self: center;
}
header.banner h1.h3, header.banner h3.h1 {
    font-size: 2.5vw;
    line-height: 3.5vw;
    font-weight: 500;
}
header img.elementos_titulo{
  width:100%;
  height:auto;
}
/* Elementos :: Titulo */

.carousel-item img{
height: auto;
min-height: 100vh;
}

.borde-redondeado{
  border-radius: 10px;
  border:1px #000 solid;
}
.emisor-titulo{
  font-size: 1.3em;
  font-weight: bold;
  color:#f21379;
}
.emisor-ruc, .emisor-numerodoc{
  font-size: 1em;
  margin-top:15px;
}
.emisor-tipodocumento{
   font-size: 1.3em;
   font-weight: bold;
   margin-top:15px;
}
.adquiriente{
  margin-top: 10px;
}
.adquiriente-titulo{
   font-size: 1.3em;
   font-weight: bold;
}
.bold{
  font-weight: bold;
}
.sup-15{
  margin-top: 15px;
}
.encabezado{
  background-color: grey;
  color:#fff;
}
.articulo{
  border-left: 1px dotted #fff;
  border-right: 1px dotted #fff;
  border-bottom: 1px dotted #fff;
}
.importe_total{
   font-size: 1.1em;
  font-weight: bold;
  color:#f21379;
}
.jumbotron {
    padding: 2rem 1rem;
    margin-bottom: 2rem;
    background-color: #e9ecef;
    border-radius: .3rem;
}
/* Fin Elementos :: Titulo */
</style>

<div class="jumbotron">

        <div class="row emisor controltotal">
          <div class="col-md-7 col-xs-12" style="background-color:red; float:left;position:relative">
            <div class="row">
            <div class="col-md-5"><img src="imagenes/logo_classic.png" class="img-fluid"></div>
            <div class="col-md-7 xoffset-md-1">
              <div class="row"><div class="col-md-12 emisor-titulo">RED CIENTIFICA PERUANA</div></div>
              <div class="row"><div class="col-md-12 emisor-direccion1"></div></div>
              <div class="row"><div class="col-md-12 emisor-direccion2"> -  - </div></div>
            </div>
            </div>
          </div>

          <div class="col-md-5 col-xs-12  borde-redondeado" style="background-color:blue; float:left;position:relative">
            <div class="row"><div class="col-md-12 emisor-ruc text-center">RUC : </div></div>
            <div class="row"><div class="col-md-12 emisor-tipodocumento text-center">FACTURA ELECTRÓNICA</div></div>
            <div class="row"><div class="col-md-12 emisor-numerodoc text-center">Nº : F003-00085540</div></div>

          </div>
        </div>


        <div class="row adquiriente">
          <div class="col-md-5 col-xs-12">
            <div class="row">
            
            <div class="col-md-12">
              <div class="row"><div class="col-md-12 adquiriente-adquiriente">Adquiriente</div></div>
              <div class="row"><div class="col-md-12 adquiriente-titulo">Xcrivas Comunicaciones y Servicios S.R.L</div></div>
              <div class="row"><div class="col-md-12 emisor-direccion1"> : </div></div>
              <div class="row"><div class="col-md-12 emisor-direccion2"></div></div>
            </div>
            </div>
          </div>

          <div class="col-md-5 col-xs-12 offset-md-2">
            <div class="row">
              <div class="col-md-6">Fecha Vencimiento</div><div class="col-md-6">Moneda</div>
              <div class="col-md-6 bold">2018-05-08</div><div class="col-md-6 bold">Soles</div>
            </div>

            <div class="row sup-15">
              <div class="col-md-6">Fecha Emisiòn</div><div class="col-md-6">IGV</div>
              <div class="col-md-6 bold">2018-05-08</div><div class="col-md-6 bold">18%</div>
            </div>
            

          </div>
        </div>


        

        <div class="row encabezado sup-15">
          <div class="col-md-1 col-xs-2">Cant:</div>
          <div class="col-md-1 col-xs-6">Unidad</div>
          <div class="col-md-5 col-xs-6">Desc</div>
          <div class="col-md-1 col-xs-2">Descuento</div>
          <div class="col-md-2 col-xs-2">Precio Unitario</div>
          <div class="col-md-2 col-xs-2">Importe Total</div>
        </div>


        
          <div class="row articulo">
          <div class="col-md-1 col-xs-2">1</div>
          <div class="col-md-1 col-xs-6">ZZ</div>
          <div class="col-md-5 col-xs-6">Registro Anual en el ccTLD.pe 08/05/2018-07/05/2019-importgam.com.pe</div>
          <div class="col-md-1 col-xs-2 text-right"></div>
          <div class="col-md-2 col-xs-2 text-right">
            93.22
        </div>
          <div class="col-md-2 col-xs-2 text-right">93.22</div>
        </div>
          



        <div class="row adquiriente">
          <div class="col-md-5 col-xs-12">
            <div class="row">
            Resumen : <strong>OwKtlEGgxmnfQvFD3ShvpwPyZ10=</strong><br>

            </div>
          </div>

          <div class="col-md-5 col-xs-12 offset-md-2">
            <div class="row">
              <div class="col-md-6">Operación Gravada</div><div class="col-md-6 text-right"></div>
              <div class="col-md-6">Operación Inafecta</div><div class="col-md-6 bold text-right">0.00</div>
              <div class="col-md-6">Operación Exonerada</div><div class="col-md-6 bold text-right">0.00</div>
              <div class="col-md-6">Operación Gratuita</div><div class="col-md-6 bold text-right">0.00</div>
            </div>

            <div class="row sup-15">
            
            
              <div class="col-md-6">Sub Total</div><div class="col-md-6 text-right">0.00</div>
              
              <!-- div class="col-md-6 bold">IGV</div><div class="col-md-6 bold text-right">18%</div -->
            </div>

            <div class="row">
              <div class="col-md-6 bold">IGV</div><div class="col-md-6 bold text-right">16.78 </div>
            </div>

            <div class="row">
              <div class="col-md-6 importe_total">Importe Total</div><div class="col-md-6 text-right importe_total">110.00</div>
              
            </div>
            

          </div>
        </div>





        <div class="row">
          <div class="col-md-3 col-xs-12">
            <input type="text" class="form-control" placeholder="correo electrónico" id="correo_electronico" name="correo_electronico"></div><div class="col-md-6 col-xs-12">           
          </div>
          <div class="col-md-3 col-xs-12" id="mensajes_correo"></div>

        </div>
        <a class="btn btn-lg btn-primary" href="#" role="button" id="enviar_por_correo">Enviar por Correo »</a>
        <a class="btn btn-lg btn-primary" href="descargar.php?documento_tipo=01&amp;documento_serie=F003-00085540&amp;documento_monto=110.00" role="button">Descargar PDF »</a>
        <a class="btn btn-lg btn-primary descargar_xml" href="#" role="button">Descargar XML »</a>
      </div>
      ';

$dompdf->loadHtml($html);
// (Optional) Setup the paper size and orientation
//$dompdf->setPaper('A4', 'landscape');
$dompdf->setPaper('A4', 'landscape');
// Render the HTML as PDF
$dompdf->render();
// Output the generated PDF to Browser y downloader
$dompdf->stream("demo.pdf",array("Attachment"=>0));
// Output the generated PDF save in path local
//$output = $dompdf->output();
//file_put_contents("file.pdf", $output);
?>