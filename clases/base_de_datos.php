<?php


class Basededatos{
	public $url;
	public $url_pdf;
	public $url_logotipo;

	public function is_pdf($filename) {
    return (file_get_contents($filename, false, null, 0, strlen(PDF_MAGIC)) === PDF_MAGIC) ? true : false;
	}

public function generar_insert_factura(){
global $enlace;
global $facturacion;
global $database_tabla;
	
	//echo "ID obtenido hash ".$id."<br>";
	$resultado="INSERT INTO `".$database_tabla."` 
	(`SERIEDOCUMENTO`, `TIPODOCUMENTO`, `FECHA`, `MONTO_TOTAL`, `SUNATXML`) 
	VALUES 
	(	'".$enlace->real_escape_string(htmlentities($facturacion->datos_factura["numero_factura"], ENT_QUOTES, "UTF-8"))."',
		'".$enlace->real_escape_string(htmlentities($facturacion->datos_factura["tipo_documento"], ENT_QUOTES, "UTF-8"))."',
		'".$enlace->real_escape_string(htmlentities($facturacion->datos_factura["fecha_emision"], ENT_QUOTES, "UTF-8"))."',
		'".$enlace->real_escape_string(htmlentities($facturacion->monto["monto_total"], ENT_QUOTES, "UTF-8"))."',
		'".$enlace->real_escape_string($facturacion->datosXML)."')";

return $resultado;
}
public function consultar($documento_tipo,$documento_serie,$documento_monto,$documento_fecha=NULL){
	global $enlace;
	global $facturacion;
	global $codigo;
	global $tipo_basedatos;
	global $database_tabla;
	$devolver=0;

	    if($tipo_basedatos==1){
	    //	echo "BASE DE DATOS 1";
	    $sql='SELECT * FROM  `'.$database_tabla.'` WHERE TIPODOCUMENTO ="'.$documento_tipo.'" AND SERIEDOCUMENTO ="'.$documento_serie.'" AND MONTO_TOTAL ="'.$documento_monto.'"';
		//echo $sql;
        $enlace->query("SET NAMES utf8");
  //      echo "Despues de Query";


        $sql_username_check = $enlace->query($sql); 
        $username_check = $sql_username_check->num_rows;
        //echo "SQL check".$sql_username_check."<br>";
   //     echo "U SQL check".$username_check."<br>";

            if($username_check > 0){
                $devolver=1;
                 while($row = $sql_username_check->fetch_array(MYSQLI_ASSOC)){
         		 $resultado=$row["SUNATXML"];
         		 //echo "RESULTADO RECIBIDO".$resultado;
         		 $facturacion->datosXML=$resultado;
         		 $facturacion->procesar();

              	}
                
            }
        }
        if($tipo_basedatos==2){
        	/* 22222222222222 */
        	//echo "BASE DE DATOS 2";
        	$sql="SELECT * FROM  ".$database_tabla." WHERE TIPODOCUMENTO='".$documento_tipo."' AND SERIEDOCUMENTO='".$documento_serie."' AND MONTO_TOTAL='".$documento_monto."'";
        	//echo $sql;
			$stmt = sqlsrv_query( $enlace, $sql );
			if( $stmt === false) {
			    die( print_r( sqlsrv_errors(), true) );
			}
			//$cantidad_resultados=sqlsrv_num_rows($stmt);
			$rows = sqlsrv_has_rows( $stmt );
		
			//echo "<br>rows ".$rows;
			//echo "<br>Cantidad de resultados ".$cantidad_resultados;
			if($rows){
				$devolver=1;
				while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
			      $resultado=$row["SUNATXML"];
			      $facturacion->datosXML=$resultado;
         		  $facturacion->procesar();
				}	
			}
			

        	/* 22222222222222 */
        }
	return $devolver;

}


public function verificar_pdf($nombre="licencia",$id="licencia"){
//print_r($_FILES);
//print_r($_FILES["licencia"]);
//print_r($_FILES[$nombre]);
//foreach ($_FILES["licencia"]["error"] as $j) {
$true=1;
if($true){

   if ($_FILES[$nombre]["error"] == UPLOAD_ERR_OK) {
       $html.="$error_codes[$error]";
               


                $archivo = $_FILES[$nombre]["tmp_name"]; 
                $tamanio = $_FILES[$nombre]["size"];
                $tipo    = $_FILES[$nombre]["type"];
                $nombre  = $_FILES[$nombre]["name"];
				$destino = $directorio."/".$nombre;
		$pos = strrpos(dirname(__FILE__) , "\\" );
		  if ($pos === false)
    	  {

	      }else{
		//	  echo "WINDOWS";
	      	//$html.="Tamos en Windows, tamos K-gaos";
			$destino = str_replace("/", "\\",$destino);
			$destinox = str_replace("/", "\\", $destinox);
			$normal = str_replace("/", "\\", $normal);
			$mini = str_replace("/", "\\", $mini);
			  }
		$seguir=1;
		if (is_uploaded_file($archivo) && $seguir==1 ) {
			//echo "Archivooo ";
			$resultado= $this->is_pdf($archivo);
			$html.=$resultado;

		}
		}
		}
return $resultado;
}	

public function obtener_valor_tabla($tabla,$columna,$valor,$retornar){
	global $enlace;
	$devolver=0;
	    $sql='SELECT * FROM  `'.$tabla.'` WHERE '.$columna.' ="'.$valor.'"';
        $enlace->query("SET NAMES utf8");
        $sql_username_check = $enlace->query($sql); 
        $username_check = $sql_username_check->num_rows;
            if($username_check > 0){
                    while($row = $sql_username_check->fetch_array(MYSQLI_ASSOC)){
         		 $resultado=$row[$retornar];
              }

                
            }

	return $resultado;
}
public function hash_verificar($hash,$tabla,$columna){
	global $enlace;
	$devolver=0;
	    $sql='SELECT * FROM  `'.$tabla.'` WHERE '.$columna.' ="'.$hash.'"';
        $enlace->query("SET NAMES utf8");
        $sql_username_check = $enlace->query($sql); 
        $username_check = $sql_username_check->num_rows;
            if($username_check > 0){
                $devolver=1;
                
            }

	return $devolver;

}

public function hash_generar($cantidad){
			$length=$cantidad;
	        if($length>0){ 
            $rand_id="";
               for($i=1; $i<=$length; $i++){
               //mt_srand((double)microtime() * 1000000);
               $num = mt_rand(1,36);
               $rand_id .= Utilitarios::assign_rand_value($num);
               }
        }
     //  echo "valor ".$rand_id."<br>";
        return $rand_id;

	
}
public function hash_obtener_id($hash,$tabla){

  global $enlace;
    
    $sql='SELECT id,hash from `'.$tabla.'` WHERE hash="'.$hash.'"';
    echo "SQL hash ".$sql."<br>";
    $enlace->query("SET NAMES utf8");
    $sql_username_check = $enlace->query($sql); 
    $username_check = $sql_username_check->num_rows;
if($username_check > 0){

    while($row = $sql_username_check->fetch_array(MYSQLI_ASSOC)){
          $valor=$row["id"];
              }


}else{$valor=0;
    }
    return $valor;
}



public function ejecutar_sentencia($sentencia){
	global $enlace;
	$devolver=0;
 	$enlace->query("SET NAMES utf8");
	  if ($resultado = $enlace->query($sentencia)) {
                $devolver=1;
                
                }

	return $devolver;        
}

public function maximo($tabla,$id="id"){
	global $enlace;
	$max=0;
	$sql='SELECT MAX( '.$id.' ) AS max FROM  `'.$tabla.'` ';
		$enlace->query("SET NAMES utf8");
    $sql_username_check = $enlace->query($sql); 
    $username_check = $sql_username_check->num_rows;

if($username_check > 0){
	 while ($row = $sql_username_check->fetch_array(MYSQLI_ASSOC)){
		$max=$row["max"];
	}
}
return $max+1;
	}	


public function cadena_verificada($nombre,
$tipo="varchar",
$longitud="2048",
$nombre_request="",
$nulo="1",
$si_vacio="",
$si_vacio_valor="1",
$si_cero="0",
$si_cero_valor="1",
$funcion_valor_retorno="",
$funcion_valor_ingreso="",
$funcion_valor_actualizar="",
$funcion_verificar_activado="0",
$funcion_verificar=""
	){
	global $enlace;
	$basededatos=new Basededatos();
	$cadena_verificada="";
	$valor="";
/*	echo "
	EL VALOR DE SI CERO VALOR  ".$valor." zzz".$si_cero_valor." SCV


	";  */
/* Primera Verificacion */
	if(@isset($_POST[$nombre])){
		// POST CORRECTO
		// echo "\$_POST[$nombre]  ".$_POST[$nombre]."		";  
		switch($tipo){
			case "varchar":
			//echo "Varchar xxx valor de vacio 			";
					if($si_vacio=="1"){
						// echo "EL VALOR DE 22222 VALOR ES ".$valor." zzz".$si_vacio_valor." SVV";
						if(empty($_POST[$nombre])){$valor=$si_vacio_valor;}else{$valor=$_POST[$nombre];}
					}else{
						$valor=$_POST[$nombre];
						// echo "EL VALOR DE VALOR ES ".$valor."						";
					}

					if(!empty($funcion_valor_ingreso)){
						switch($funcion_valor_ingreso){
							case "encriptar":
							$valor=$basededatos->encriptar($_POST[$nombre]);
							break;
							//$valor=call_user_func($funcion_valor_ingreso, $_POST[$nombre]);
							default:
							$valor=call_user_func($funcion_valor_ingreso, $_POST[$nombre]);
							break;
						}
						
					}
			break;
			case "date":
			//echo "Varchar xxx valor de vacio 			";
					if($si_vacio=="1"){
						// echo "EL VALOR DE 22222 VALOR ES ".$valor." zzz".$si_vacio_valor." SVV";
						if(empty($_POST[$nombre])){$valor=$si_vacio_valor;}else{$valor=$_POST[$nombre];}
					}else{
						$valor=$_POST[$nombre];
						// echo "EL VALOR DE VALOR ES ".$valor."						";
					}
			break;
			case "int":
		//	echo "Int xxx 			";
			// echo "EL VALOR DE 22222 VALOR ES ".$valor." zzz".$si_cero_valor." SCV
			// ";
				if(Utilitarios::chequear($_POST[$nombre])){

					if($si_cero=="1"){
						// echo " Se llego si cero";
						if($_POST[$nombre]=="0"){$valor=$si_cero_valor;}else{$valor=$_POST[$nombre];}
					}else{
						//echo "NO ESCER".$_POST[$nombre];
						$valor=$_POST[$nombre];
						//echo "VDV".$valor;
					}
				}else{
					switch($nombre){
						case "pais":
						$valor=Paises::obteneridpais($_POST[$nombre]);
						break;
						case "facturacion_pais":
						$valor=Paises::obteneridpais($_POST[$nombre]);
						break;
						case "paisresidencia":
						$valor=Paises::obteneridpais($_POST[$nombre]);
						break;
						case "tarjeta_pais":
						$valor=Paises::obteneridpais($_POST[$nombre]);
						break;
						case "nacionalidad":
						$valor=Paises::obteneridpais($_POST[$nombre]);
						break;
						case "pasaporte_pais":
						$valor=Paises::obteneridpais($_POST[$nombre]);
						break;
					}
				}

			break;
		}

	}
	else{

			switch($tipo){
			case "varchar":
					$valor=$si_vacio_valor;
			break;
			case "int":
				$valor=$si_cero_valor;
			break;
		}

	}
//	if(@empty($nombre_request)){ $valor=$_POST[$$nombre]; }else{ $valor=$_POST[$$nombre_request];}

/* Fin Primera verificacion */

//$enlace->real_escape_string($valor);
$valor=$enlace->real_escape_string(htmlentities($valor, ENT_QUOTES, "UTF-8"));
$cadena_verificada="'".$valor."'";
/* echo "
VALOR return ".$cadena_verificada."
"; */
return	 $cadena_verificada;

}


}
?>