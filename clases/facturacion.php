<?php
/**
SISTEMA FACTURACIÓN ELECTRÓNICA v 0.0001
Por          TELEFACTURACIÓN.COM 
RAZÓN SOCIAL TELEFACTURACIÓN S.A.C
DIRECCIÓN    PORTINARI 196 - SAN BORJA
TELEFONO     930222760 - 012232549 - 992233565 
DESARROLLO   CON VOLT y NINGUN java.Lang.NullPointerException



*/

//$this->tipo_documento[$this->datos_factura["tipo_documento"]]
class Facturacion{
	public $datosXML;
	public $datosXMLinvoice;
	public $monto;
	public $articulos;
	public $datos_factura;
	public $tipo_tributo=array(
		"1000"=>"IGV",
		"2000"=>"ISC",
		"9995"=>"EXPORTACION",
		"9996"=>"GRATUITO",
		"9997"=>"EXONERADO",
		"9998"=>"INAFECTO",
		"9999"=>"OTROS"
		);

	public $tipo_doc_identidad=array(
		"0"=>"No Domiciliado / SIN R.U.C",
		"1"=>"D.N.I",
		"4"=>"C.E",
		"6"=>"R.U.C",
		"7"=>"Pasaporte",
		"A"=>"Ced.Diplomatica de Identidad",
		"B"=>"Doc Identidad Pais residencia / No Domiciliado",
		"C"=>"Tax identificacion number, Persona Natural",
		"D"=>"Identification number, Persona Juridica"
		);
	public $tipo_documento=array(
								"01"=>"Factura",
              "03"=>"Boleta de venta",
              "07"=>"Nota de Crédito",
              "08"=>"Nota de Debito",
              "09"=>"Guia de remisión remitente",
              "12"=>"Ticket de maquina registradora",
              "13"=>"Doc. emitido por Bancos",
              "14"=>"Recibo Servicios Publicos",
              "18"=>"Documentos emitidos por las AFP",
              "31"=>"Guia de Remisión Transportista",
              "56"=>"Comprobante de pago SEAE",
              "71"=>"Guia de Remisión remitente Complementaria",
              "72"=>"Guia de Remisión transportista complementaria"
		);

		public $tipo_moneda=array(
			  "PEN"=>"Soles",
              "USD"=>"Dólar Estadounidense",
              "BOB"=>"Boliviano",
              "COP"=>"Peso Colombiano",
              "BRL"=>"Real Brasileño",
              "EUR"=>"Euro",
              "VEF"=>"Bolivar",
              "RUB"=>"Rublo Ruso",
              "CNY"=>"Yuan",
              "JPY"=>"Yen",
              "PYG"=>"Guaraní",
              "GBP"=>"Libra Esterlina"
		);
	public $TaxTypeCode = array(
							"1000" => "impuestos_igv",
							"2000" => "impuestos_isc",
							"9999" => "impuestos_otros"
						);
	public $captura=array(
						"cbcID" => array(
							"1001" => "operaciones_gravadas",
							"1002" => "operaciones_inafectas",
							"1003" => "operaciones_exoneradas",
							"1004" => "operaciones_gratuitas",
							"1005" => "subtotal",
							"2001" => "percepciones",
							"2002" => "retenciones",
							"2003" => "detracciones",
							"2004" => "bonificaciones",
							"2005" => "total_descuentos",
							"3001" => "FISE"
							/*,"1000" => "monto_en_letras",*/
						),
						"item" => array(
							"cbc:InvoicedQuantity" => "articulo_cantidad",
							"cbc:LineExtensionAmount" => "articulo_subtotal",
							"cac:PricingReference" => "articulo_total",
							"cac:TaxTotal" => "articulo_impuesto",
							"cac:Item" => "articulo_descripcion",
							"cac:Price" => "articulo_precio_unitario"
						)
		);
	/* */
	public function generar_qr($ruc,$tipo_documento,$serie,$numero,$igv,$total,$fecha_emision,$adquiriente_tipo_doc,$adquiriente_num_doc){
	include('./_phpqrcode/qrlib.php');
   // how to save PNG codes to server
    $contenido = $ruc.'|'.$tipo_documento.'|'.$serie.'|'.$numero.'|'.$igv.'|'.$total.'|'.$fecha_emision.'|'.$adquiriente_tipo_doc.'|'.$adquiriente_num_doc.'|';

    $archivo = $serie.'-'.$numero.'.png';
    $directorio='qr/';
    $ruta = $directorio.$archivo;
        
    // generating
    if (!file_exists($ruta)) {
        QRcode::png($contenido, $ruta);
    } else {
    	//QRcode::png($contenido, $ruta);
    }
    // displaying
    
    return $ruta;
}
	/* */
public function Facturacion(){
	//echo "LLAMADA AUTOMAICA";
	$this->monto["operaciones_gravadas"]="0.00";
$this->monto["operaciones_inafectas"]="0.00";
$this->monto["operaciones_exoneradas"]="0.00";
$this->monto["operaciones_gratuitas"]="0.00";
$this->monto["total_descuentos"]="0.00";
$this->monto["subtotal"]="0.00";

$this->monto["monto_operaciones_igv"]="0.00";
$this->monto["monto_operaciones_gravadas"]="0.00";
$this->monto["monto_operaciones_isc"]="0.00";
$this->monto["monto_operaciones_exportacion"]="0.00";
$this->monto["monto_operaciones_gratuitas"]="0.00";
$this->monto["monto_operaciones_exoneradas"]="0.00";							
$this->monto["monto_operaciones_inafecto"]="0.00";

}
public function datosXMLinvoicex($arreglo){
$temp_dom = new DOMDocument();
//echo "Hola";
foreach($arreglo as $n) $temp_dom->appendChild($temp_dom->importNode($n,true));
//print_r($temp_dom->saveXML());
$this->datosXMLinvoice=$temp_dom->saveXML();

}
public function procesar(){
	global $codigo;
global $facturacion;
global $espacio_nombres;
$doc = new DOMDocument();
$doc->loadXML($this->datosXML);
$xpath = new DOMXPath($doc);
$xpath->registerNamespace("neogrid","http://www.neogrid.com");
$xpath->registerNamespace("principal","urn:oasis:names:specification:ubl:schema:xsd:Invoice-2");
$xpath->registerNamespace("cac","urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2");
$xpath->registerNamespace("cbc","urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2");
$xpath->registerNamespace("ccts","urn:un:unece:uncefact:documentation:2");
$xpath->registerNamespace("sac","urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1");
$xpath->registerNamespace("ext","urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2");
$xpath->registerNamespace("ds","http://www.w3.org/2000/09/xmldsig#");

$consulta =$espacio_nombres.'/principal:Invoice/cbc:IssueDate';

$array = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:AllowanceCharge');
$res=$facturacion->obtener_descuento_global($array);

$array = $xpath->evaluate($espacio_nombres.'/principal:Invoice/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/ds:Signature/ds:SignedInfo/ds:Reference/ds:DigestValue');
$res=$facturacion->obtener_digestvalue($array);

$array = $xpath->evaluate($espacio_nombres.'/principal:Invoice/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sac:AdditionalInformation/sac:AdditionalMonetaryTotal');
//$facturacion=new facturacion();
$res=$facturacion->obtener_valores($array);


$array = $xpath->evaluate($espacio_nombres.'/principal:Invoice');
//$facturacion=new facturacion();
$res=$facturacion->datosXMLinvoicex($array);


$array = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:TaxTotal');
$res=$facturacion->obtener_impuestos($array);


$array = $xpath->evaluate($espacio_nombres.'/principal:Invoice/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sac:AdditionalInformation/sac:AdditionalProperty');
$res=$facturacion->obtener_monto_en_letras($array);
//print_r($facturacion->monto);
$otros_datos=$facturacion->datos_factura($xpath);
//print_r($facturacion->datos_factura);
$articulos=$facturacion->articulos($xpath);

//$facturacion->mostrar_factura();
//print_r($facturacion->captura);
//print_r($facturacion->articulos);



}
	 
						
		

	public function datos_factura($xpath){
		/*$this->monto["aaa22"]="bbb";
		print_r($this->monto);*/
		global $espacio_nombres;
		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cbc:ID');
		foreach ($arreglo as $entrada) { $this->datos_factura["numero_factura"]=$entrada->nodeValue ;}
		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cbc:IssueDate');
		foreach ($arreglo as $entrada) { $this->datos_factura["fecha_emision"]=$entrada->nodeValue ;}
		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cbc:InvoiceTypeCode');
		foreach ($arreglo as $entrada) { $this->datos_factura["tipo_documento"]=$entrada->nodeValue ;}
		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cbc:DocumentCurrencyCode');
		foreach ($arreglo as $entrada) { $this->datos_factura["moneda"]=$entrada->nodeValue ;}

		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyLegalEntity/cbc:RegistrationName');
		foreach ($arreglo as $entrada) { $this->datos_factura["emisor"]=$entrada->nodeValue ;}

		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification/cbc:ID');
		//$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:AccountingSupplierParty/cbc:CustomerAssignedAccountID');
		foreach ($arreglo as $entrada) { $this->datos_factura["emisor_ruc"]=$entrada->nodeValue ;}


		/*cac:AccountingSupplierParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:District
		cac:AccountingSupplierParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:CountrySubentity
		cac:AccountingSupplierParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:CityName*/
		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cac:AddressLine/cbc:Line');
		foreach ($arreglo as $entrada) { $this->datos_factura["emisor_direccion"]=$entrada->nodeValue ;}
		
		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:District');
		foreach ($arreglo as $entrada) { $this->datos_factura["emisor_distrito"]=$entrada->nodeValue ;}

		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:CountrySubentity');
		foreach ($arreglo as $entrada) { $this->datos_factura["emisor_provincia"]=$entrada->nodeValue ;}

		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:CityName');
		foreach ($arreglo as $entrada) { $this->datos_factura["emisor_departamento"]=$entrada->nodeValue ;}


		/*$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PostalAddress/cbc:StreetName');
		foreach ($arreglo as $entrada) { $this->datos_factura["emisor_direccion"]=$entrada->nodeValue ;}
		
		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PostalAddress/cbc:District');
		foreach ($arreglo as $entrada) { $this->datos_factura["emisor_distrito"]=$entrada->nodeValue ;}

		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PostalAddress/cbc:CountrySubentity');
		foreach ($arreglo as $entrada) { $this->datos_factura["emisor_provincia"]=$entrada->nodeValue ;}

		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PostalAddress/cbc:CityName');
		foreach ($arreglo as $entrada) { $this->datos_factura["emisor_departamento"]=$entrada->nodeValue ;}

*/
		//$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:AccountingCustomerParty/cbc:CustomerAssignedAccountID');
		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification/cbc:ID');
		foreach ($arreglo as $entrada) { $this->datos_factura["cliente_ruc"]=$entrada->nodeValue ;
//		echo "atributos :".$entrada->attributes;
		$cosa=$entrada->getAttribute('schemeID');
		$cosa2=$entrada->getAttribute('schemeName');
		$this->datos_factura["cliente_tipo_documento_cod"]=$cosa;
		$this->datos_factura["cliente_tipo_documento_nombre"]=$cosa2;
		/*echo "La cosa  es :".$cosa."<br>";
		echo "La cosa2  es :".$cosa2."<br>";*/
	}

		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cbc:RegistrationName');
		foreach ($arreglo as $entrada) { $this->datos_factura["cliente_nombre"]=$entrada->nodeValue ;}

		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:StreetName');
		foreach ($arreglo as $entrada) { $this->datos_factura["cliente_direccion"]=$entrada->nodeValue ;}

$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount');
		foreach ($arreglo as $entrada) { $this->monto["monto_total"]=$entrada->nodeValue ;}

/**
Detectar si es CDATA
if ($child->nodeType == XML_CDATA_SECTION_NODE) {
            echo $child->textContent . "<br/>";
        }
*/
/*
cac:AccountingCustomerParty/cbc:CustomerAssignedAccountID
cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cbc:RegistrationName
cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress/cbc:StreetName

        <cac:AccountingCustomerParty>
          <cbc:CustomerAssignedAccountID>20517724581</cbc:CustomerAssignedAccountID>
          <cbc:AdditionalAccountID>6</cbc:AdditionalAccountID>
          <cac:Party><cac:PartyLegalEntity>
            <cbc:RegistrationName>RESTAURANT V &amp; D GRILL BERNA 2 S.A.C.</cbc:RegistrationName>
            <cac:RegistrationAddress>
              <cbc:StreetName>JR. FERNANDO FAUSTOR NRO. 299  SANTIAGO DE SURCO</cbc:StreetName>
            </cac:RegistrationAddress>
          </cac:PartyLegalEntity>
        </cac:Party>
      </cac:AccountingCustomerParty>


            */

		/*$temp_dom = new DOMDocument();
foreach($arreglo as $n) $temp_dom->appendChild($temp_dom->importNode($n,true));
print_r($temp_dom->saveHTML());
*/
	}

	public function articulos($xpath){
		global $espacio_nombres;
		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:InvoiceLine');
		$i=0;
		foreach ($arreglo as $entrada) { 

			  	$name = $entrada->nodeName;
      			$value = $entrada->nodeValue;
      			
      			//echo "Attribute '$name' :: '$value' y el tipo de atributo es  <br />";
      			$children = $entrada->childNodes;
      			
      			foreach ($children as $ccc) {
      				$name = $ccc->nodeName;
      				$value = $ccc->nodeValue;
      				switch($ccc->nodeName){
      					case 'cbc:InvoicedQuantity':
      					case 'cbc:LineExtensionAmount':
      					case 'cac:PricingReference':
      					case 'cac:Price':
      					
      						$this->articulos[$i][$this->captura["item"][$ccc->nodeName]]=$ccc->nodeValue;
      						if($ccc->nodeName=="cbc:InvoicedQuantity"){
      								$unidad_medida=$ccc->getAttribute('unitCode');
      								$this->articulos[$i]["articulo_unidad_medida"]=$unidad_medida;
      						}
      					break;
      					case 'cac:Item':
      						$nuevo_children=$ccc->childNodes;
      						foreach ($nuevo_children as $nuevo_ccc) {
      							$name = $nuevo_ccc->nodeName;
      							$value = $nuevo_ccc->nodeValue;
      							if($name=="cbc:Description"){$this->articulos[$i][$this->captura["item"]["cac:Item"]]=$nuevo_ccc->nodeValue;}
      						}
      					break;
      					
      					case 'cac:TaxTotal':
      						$nuevo_children=$ccc->childNodes;
      						foreach ($nuevo_children as $nuevo_ccc) {
      							$name = $nuevo_ccc->nodeName;
      							$value = $nuevo_ccc->nodeValue;
      							if($name=="cbc:TaxAmount"){$this->articulos[$i][$this->captura["item"]["cac:TaxTotal"]]=$nuevo_ccc->nodeValue;}
      						}
      					break;
      					

      				}
      			
      				//echo "Attribute '$name' :: '$value' y el tipo de atributo es  <br />";
      				
      			}

			//$nuevo= $entrada->evaluate('');
			//		$this->datos_factura["cliente_direccion"]=$entrada->nodeValue ;
      			$i=$i+1;
		}
		
	}

	public function obtener_valores($arreglo){
global $espacio_nombres;
		foreach ($arreglo as $entrada) {
  //echo "COCA COLA ".$entrada->nodeValue."<br>";
// print_r($entrada);
$children = $entrada->childNodes;

            /*  <sac:AdditionalMonetaryTotal>
                <cbc:ID>1001</cbc:ID>
                <cbc:PayableAmount currencyID="PEN">307.81</cbc:PayableAmount>
              </sac:AdditionalMonetaryTotal>
			*/

    foreach ($children as $child) {
    	if($child->nodeName=="cbc:ID"){ $nombre_cbcid=$child->nodeValue;}
    	if($child->nodeName=="cbc:PayableAmount"){ 
    		//$nombre_cbcid=$child->nodeValue;
    		$this->monto[$this->captura["cbcID"][$nombre_cbcid]]=$child->nodeValue;
    	}


/*      $name = $child->nodeName;
      $value = $child->nodeValue;
      $cosa=$child->getAttribute('currencyID');
      echo "Attribute '$name' :: '$value' y el tipo de atributo es $cosa <br />"; */

  } 
}


		/*$this->monto["operaciones_gravadas"]="0.00";
		$this->monto["operaciones_inafectas"]="0.00";
		$this->monto["operaciones_exoneradas"]="0.00";
		$this->monto["operaciones_gratuitas"]="0.00";
		$this->monto["subtotal"]="0.00";
		$this->monto["percepciones"]="0.00";
		$this->monto["retenciones"]="0.00";
		$this->monto["detracciones"]="0.00";
		$this->monto["bonificaciones"]="0.00";
		$this->monto["total_descuentos"]="0.00";
		$this->monto["FISE"]="0.00";
		$this->monto["monto_en_letras"]="SON MILLON de INTIS"; */
		
	}
public function obtener_descuento_global($arreglo){
		global $espacio_nombres;
		
		foreach ($arreglo as $entrada) {
			

			$children = $entrada->childNodes;
			    foreach ($children as $child) {
				//echo "<br>NOMBRE subNODO ".$child->nodeName."<br>";
				//echo "<br>vALOR SUBNODO ".$child->nodeValue."<br>"; 
				switch($child->nodeName){
					case "cbc:Amount":
					$this->monto["monto_descuento_global"]=$child->nodeValue;
					break;
					case "cbc:BaseAmount":
					$this->monto["monto_base_descuento_global"]=$child->nodeValue;
					break;
					case "MultiplierFactorNumeric":
					$this->monto["monto_descuento_global_porcentaje"]=$child->nodeValue;
					break;
				}   	
			    }
			 
		}
		//print_r($this->monto);
	}

public function obtener_digestvalue($arreglo){
		global $espacio_nombres;
		
		foreach ($arreglo as $entrada) {
			
			$this->datos_factura["digestvalue"]=$entrada->nodeValue;

			 
		}
		//print_r($this->monto);
	}

	public function obtener_impuestos($arreglo){
		global $espacio_nombres;
		/*echo "Arreglo recibido";
		print_r($arreglo); 
*/


/*
mostrar xml recibido
			$temp_dom = new DOMDocument();
			foreach($arreglo as $n) $temp_dom->appendChild($temp_dom->importNode($n,true));
			print_r($temp_dom->saveHTML());
*/




		foreach ($arreglo as $entrada) {
		//	echo "<br>NOMBRE NODO ".$entrada->nodeName."<br>";
		//	if($entrada->nodeName=="cbc:TaxAmount"){ echo "<br>TacAmount encontrado<br>";  $this->monto["total_impuestos"]=$entrada->nodeValue;}
  			$children = $entrada->childNodes;
			    foreach ($children as $child) {
			    	if($child->nodeName=="cbc:TaxAmount"){  $this->monto["total_impuestos"]=$child->nodeValue;}
			    	if($child->nodeName=="cac:TaxSubtotal"){ 
			    		//$nombre_cbcid=$child->nodeValue;
			    		//$this->monto[$this->captura["cbcID"][$nombre_cbcid]]=$child->nodeValue;
			    		$impuesto_monto="";
			    		$impuesto_tipo="";
			    		$sub_children = $child->childNodes;
			    		



			$temp_dom = new DOMDocument();
			foreach($sub_children as $n) $temp_dom->appendChild($temp_dom->importNode($n,true));
			//print_r($temp_dom->saveXML());

			$temporal=$temp_dom->saveXML();

			$temporal2 = new DOMXPath($temp_dom);
			$temporal2->registerNamespace("cac","urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2");
			$temporal2->registerNamespace("cbc","urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2");

			    		$arreglo = $temporal2->query('/cbc:TaxableAmount');
						foreach ($arreglo as $entrada) { $impuesto_monto_imponible=$entrada->nodeValue ;}

			    		$arreglo = $temporal2->query('/cbc:TaxAmount');
						foreach ($arreglo as $entrada) { $impuesto_monto=$entrada->nodeValue ;}

						$arreglo = $temporal2->query('/cac:TaxCategory/cac:TaxScheme/cbc:Name');
						foreach ($arreglo as $entrada) { $impuesto_tipo=$entrada->nodeValue ;}

						$arreglo = $temporal2->query('/cac:TaxCategory/cac:TaxScheme/cbc:ID');
						foreach ($arreglo as $entrada) { $impuesto_codigo=$entrada->nodeValue ;}

						switch($impuesto_codigo){

							case "1000":
							$this->monto["monto_operaciones_igv"]=$impuesto_monto;
							$this->monto["monto_operaciones_gravadas"]=$impuesto_monto_imponible;
							break;
							case "2000":
							$this->monto["monto_operaciones_isc"]=$impuesto_monto;
							break;
							case "9995":
							$this->monto["monto_operaciones_exportacion"]=$impuesto_monto_imponible;
							break;
							case "9996":
							$this->monto["monto_operaciones_gratuitas"]=$impuesto_monto_imponible;
							break;
							case "9997":
							$this->monto["monto_operaciones_exoneradas"]=$impuesto_monto_imponible;
							break;
							case "9998":
							$this->monto["monto_operaciones_inafecto"]=$impuesto_monto_imponible;
							break;
							case "9999":
							$this->monto["monto_operaciones_otros"]=$impuesto_monto_imponible;
							break;
								}


						if($impuesto_tipo=="INAFECTO"){$this->monto["impuesto_".$impuesto_tipo]=$impuesto_monto_imponible;
						}
						else{
							$this->monto["impuesto_".$impuesto_tipo]=$impuesto_monto;
						}
						$this->monto["impuesto"][]=array(
													"monto_imponible" =>$impuesto_monto_imponible,
													"monto" =>$impuesto_monto,
													"tipo" =>$impuesto_tipo
													);
			    		/* Prueba captura */

			//    		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PostalAddress/cbc:StreetName');
			//			foreach ($arreglo as $entrada) { $this->datos_factura["emisor_direccion"]=$entrada->nodeValue ;}

			    		/* FIN Prueba captura*/



			    			foreach ($sub_children as $sub_child) {
			    			}


			    	}
			  } 
		}
		//print_r($this->monto);
	}

    /*  <cac:TaxTotal>
        <cbc:TaxAmount currencyID="PEN">55.41</cbc:TaxAmount>

        <cac:TaxSubtotal>
          <cbc:TaxAmount currencyID="PEN">55.41</cbc:TaxAmount>
          <cac:TaxCategory>
            <cac:TaxScheme>
              <cbc:ID>1000</cbc:ID>
              <cbc:Name>IGV</cbc:Name>
              <cbc:TaxTypeCode>VAT</cbc:TaxTypeCode>
            </cac:TaxScheme>
          </cac:TaxCategory>
        </cac:TaxSubtotal>
      </cac:TaxTotal>*/


	public function obtener_monto_en_letras($arreglo){
		global $espacio_nombres;
	/*	echo "Arreglo recibido";
		print_r($arreglo); */
		/* 		$this->datos_factura["monto_total2"]="cuca";
		$this->monto["Operacions_extrañas"]="12.00";
		$arreglo = $xpath->evaluate($espacio_nombres.'/principal:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount');
		foreach ($arreglo as $entrada) { $this->monto["monto_total"]=$entrada->nodeValue ;}*/
		$this->monto["aaa"]="bbb";
		foreach ($arreglo as $entrada) {
  			$children = $entrada->childNodes;
			    foreach ($children as $child) {
			    	if($child->nodeName=="cbc:ID"){ $nombre_cbcid=$child->nodeValue;}
			    	if($child->nodeName=="cbc:Value"){ 
			    		//$nombre_cbcid=$child->nodeValue;
			    		$this->monto[$this->captura["cbcID"][$nombre_cbcid]]=$child->nodeValue;
			    	}
			  } 
		}
	}

	public function mostrar_factura(){
		global $espacio_nombres;
		global $correo_servicio_activo;
		//print_r($this->datos_factura);
		//$resultado="vaca";
		//Otro Cambio
		//Cambio1
		//echo $resultado;
		//<div class="col-md-6 bold">ISC</div><div class="col-md-6 bold text-right"> </div>
		//print_r($this->monto["impuesto"]);

		foreach($this->monto["impuesto"] as $tipo_impuesto){
			if(floatval($tipo_impuesto["monto"])!=0){$impuestos_texto='<div class="col-md-6 bold">'.$tipo_impuesto["tipo"].'</div><div class="col-md-6 bold text-right">'.$tipo_impuesto["monto"].' </div>';}
			

		}

		/*         <div class="row encabezado sup-15">
          <div class="col-md-1 col-xs-2">Cant:</div>
          <div class="col-md-1 col-xs-6">Unidad</div>
          <div class="col-md-5 col-xs-6">Desc</div>
          <div class="col-md-1 col-xs-2">Descuento</div>
          <div class="col-md-2 col-xs-2">Precio Unitario</div>
          <div class="col-md-2 col-xs-2">Importe Total</div>
        </div>


        <div class="row articulo">
          <div class="col-md-1 col-xs-2">1.00</div>
          <div class="col-md-1 col-xs-6">UZZ</div>
          <div class="col-md-5 col-xs-6">Lote Cuadernos Shark para Colegio, 89 Hojas, Linea triple en hoja Morada</div>
          <div class="col-md-1 col-xs-2 text-right"></div>
          <div class="col-md-2 col-xs-2 text-right">110.00</div>
          <div class="col-md-2 col-xs-2 text-right">110.00</div>
        </div>
        */
        $this->monto["monto_subtotal"]=
							floatval($this->monto["monto_operaciones_gravadas"])+
							floatval($this->monto["monto_operaciones_exportacion"])+
							floatval($this->monto["monto_operaciones_gratuitas"])+
							floatval($this->monto["monto_operaciones_exoneradas"])+
							floatval($this->monto["monto_operaciones_inafecto"]);
							$this->monto["monto_subtotal"]=number_format((float)$this->monto["monto_subtotal"], 2, '.', '');
        foreach($this->articulos as $articulo){
        	$articulo_texto.='
        	<div class="row articulo">
          <div class="col-md-1 col-xs-2">'.$articulo["articulo_cantidad"].'</div>
          <div class="col-md-1 col-xs-6">'.$articulo["articulo_unidad_medida"].'</div>
          <div class="col-md-5 col-xs-6">'.$articulo["articulo_descripcion"].'</div>
          <div class="col-md-1 col-xs-2 text-right"></div>
          <div class="col-md-2 col-xs-2 text-right">'.$articulo["articulo_precio_unitario"].'</div>
          <div class="col-md-2 col-xs-2 text-right">'.$articulo["articulo_subtotal"].'</div>
        </div>
        	';

        }
if(floatval($this->monto["total_descuentos"])!=0){
	$descuentos='<div class="col-md-6">Total Descuentos</div><div class="col-md-6 text-right">'.$this->monto["total_descuentos"].'</div>';
}
if(floatval($this->monto["monto_descuento_global"])!=0){
 $descuentos='<div class="col-md-6">Descuentos Globales</div><div class="col-md-6 text-right">'.$this->monto["monto_descuento_global"].'</div>';
        }
        /* Generar imagen QR */
        /* 
        $this->datos_factura["emisor_ruc"]
        $this->datos_factura["tipo_documento"]
        $this->datos_factura["tipo_documento"] SEPARARLO POR GUION
        $this->monto["monto_operaciones_igv"]
        $this->monto["monto_total"]
        $this->datos_factura["fecha_emision"]
        $this->datos_factura["cliente_tipo_documento_cod"]
        $this->datos_factura["cliente_ruc"]



        $this->datos_factura["emisor_ruc"],
        $this->datos_factura["tipo_documento"],
        $porciones[0],
        $porciones[1],
        $this->monto["monto_operaciones_igv"],
        $this->monto["monto_total"],
        $this->datos_factura["fecha_emision"],
        $this->datos_factura["cliente_tipo_documento_cod"],
        $this->datos_factura["cliente_ruc"]

        */
        $porciones = explode("-", $this->datos_factura["numero_factura"]);
        $resultado=$this->generar_qr($this->datos_factura["emisor_ruc"],$this->datos_factura["tipo_documento"],$porciones[0],$porciones[1], $this->monto["monto_operaciones_igv"], $this->monto["monto_total"], $this->datos_factura["fecha_emision"], $this->datos_factura["cliente_tipo_documento_cod"], $this->datos_factura["cliente_ruc"]);
        //$resultado=$this->generar_qr("20374656741","01","F001","00000007","19.50","141.50","2018-08-02","01","10481484524");
        $imagen_qr='<img src="'.$resultado.'">';
        /* FIN GENERAR IMAGEN QR */
		$resultado=' <div class="row text-center">

      <!-- Imagen superior removida -->
      <!-- img src="imagenes/logo_classic.png" class="img-responsive img-fluid center-block" style="margin:auto;"   -->
      </div>
      <div class="jumbotron">

        <div class="row emisor controltotal">
          <div class="col-md-7 col-xs-12">
            <div class="row">
            <div class="col-md-5"><img src="imagenes/logo_classic.png" class="img-fluid"></div>
            <div class="col-md-7 xoffset-md-1">
              <div class="row"><div class="col-md-12 emisor-titulo">'.$this->datos_factura["emisor"].'</div></div>
              <div class="row"><div class="col-md-12 emisor-direccion1">'.$this->datos_factura["emisor_direccion"].'</div></div>
              <div class="row"><div class="col-md-12 emisor-direccion2">'.$this->datos_factura["emisor_distrito"].' - '.$this->datos_factura["emisor_provincia"].' - '.$this->datos_factura["emisor_departamento"].'</div></div>
            </div>
            </div>
          </div>

          <div class="col-md-5 col-xs-12  borde-redondeado">
            <div class="row"><div class="col-md-12 emisor-ruc text-center">RUC : '.$this->datos_factura["emisor_ruc"].'</div></div>
            <div class="row"><div class="col-md-12 emisor-tipodocumento text-center">'.strtoupper($this->tipo_documento[$this->datos_factura["tipo_documento"]]." ELECTRÓNICA").'</div></div>
            <div class="row"><div class="col-md-12 emisor-numerodoc text-center">Nº : '.$this->datos_factura["numero_factura"].'</div></div>

          </div>
        </div>


        <div class="row adquiriente">
          <div class="col-md-5 col-xs-12">
            <div class="row">
            
            <div class="col-md-12">
              <div class="row"><div class="col-md-12 adquiriente-adquiriente">Adquiriente</div></div>
              <div class="row"><div class="col-md-12 adquiriente-titulo">'.$this->datos_factura["cliente_nombre"].'</div></div>
              <div class="row"><div class="col-md-12 emisor-direccion1">'.$this->tipo_doc_identidad[$this->datos_factura["cliente_tipo_documento_cod"]].' : '.$this->datos_factura["cliente_ruc"].'</div></div>
              <div class="row"><div class="col-md-12 emisor-direccion2">'.$this->datos_factura["cliente_direccion"].'</div></div>
            </div>
            </div>
          </div>

          <div class="col-md-5 col-xs-12 offset-md-2">
            <div class="row">
              <div class="col-md-6">Fecha Vencimiento</div><div class="col-md-6">Moneda</div>
              <div class="col-md-6 bold">'.$this->datos_factura["fecha_emision"].'</div><div class="col-md-6 bold">'.$this->tipo_moneda[$this->datos_factura["moneda"]].'</div>
            </div>

            <div class="row sup-15">
              <div class="col-md-6">Fecha Emisiòn</div><div class="col-md-6">IGV</div>
              <div class="col-md-6 bold">'.$this->datos_factura["fecha_emision"].'</div><div class="col-md-6 bold">18%</div>
            </div>
            

          </div>
        </div>


        

        <div class="row encabezado sup-15">
          <div class="col-md-1 col-xs-2">Cant:</div>
          <div class="col-md-1 col-xs-6">Unidad</div>
          <div class="col-md-5 col-xs-6">Desc</div>
          <div class="col-md-1 col-xs-2">Descuento</div>
          <div class="col-md-2 col-xs-2">Precio Unitario</div>
          <div class="col-md-2 col-xs-2">Importe Total</div>
        </div>


        '.$articulo_texto.'



        <div class="row adquiriente">
          <div class="col-md-5 col-xs-12">
            <div class="row">
            Resumen : <strong>'.$this->datos_factura["digestvalue"].'</strong><br>

            </div>
            <div class="row" style="margin-top:8px;">'.$imagen_qr.'</div>
          </div>

          <div class="col-md-5 col-xs-12 offset-md-2">
            <div class="row">
              <div class="col-md-6">Operación Gravada</div><div class="col-md-6 text-right">'.$this->monto["monto_operaciones_gravadas"].'</div>
              <div class="col-md-6">Operación Inafecta</div><div class="col-md-6 bold text-right">'.$this->monto["monto_operaciones_inafecto"].'</div>
              <div class="col-md-6">Operación Exonerada</div><div class="col-md-6 bold text-right">'.$this->monto["monto_operaciones_exoneradas"].'</div>
              <div class="col-md-6">Operación Gratuita</div><div class="col-md-6 bold text-right">'.$this->monto["monto_operaciones_gratuitas"].'</div>
            </div>

            <div class="row sup-15">
            '.$descuentos.'
            
              <div class="col-md-6">Sub Total</div><div class="col-md-6 text-right">'.$this->monto["monto_subtotal"].'</div>
              
              <!-- div class="col-md-6 bold">IGV</div><div class="col-md-6 bold text-right">18%</div -->
            </div>

            <div class="row">
              '.$impuestos_texto.'
            </div>

            <div class="row">
              <div class="col-md-6 importe_total">Importe Total</div><div class="col-md-6 text-right importe_total">'.$this->monto["monto_total"].'</div>
              
            </div>
            

          </div>
        </div>





        <div class="row">
        	<div class="col-md-3 col-xs-12">
        		'.$correo_input.'        		
        	</div>
        	<div class="col-md-3 col-xs-12" id="mensajes_correo"></div>

        </div>
        '.$correo_boton.'
        <a class="btn btn-lg btn-primary" href="descargar.php?documento_tipo='.$this->datos_factura["tipo_documento"].'&documento_serie='.$this->datos_factura["numero_factura"].'&documento_monto='.$this->monto["monto_total"].'" role="button">Descargar PDF &raquo;</a>
        <a class="btn btn-lg btn-primary descargar_xml" href="#" role="button">Descargar XML &raquo;</a>
      </div>
      ';
      $datosXMLinvoicesimple=addslashes($this->datosXMLinvoice);
      $datosXMLinvoicesimple = str_replace(array("\r\n", "\n", "\r"), ' ', $datosXMLinvoicesimple);
      $nombre_factura=$this->datos_factura["numero_factura"].".xml";
      $tipo_documento_xx=$this->datos_factura["tipo_documento"];
      $numero_factura_xx=$this->datos_factura["numero_factura"];
      $monto_total_xx=$this->monto["monto_total"];
       $resultado .= <<<EOF
       <script>
       function callback(){
       	console.log("hecho");
       }
   function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/xml;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

// Start file download.
//download("$nombre_factura","$datosXMLinvoicesimple");

$( document ).ready(function() {
    console.log( "ready!" );


$('body').on('click', '.descargar_xml', function(b){
	download("$nombre_factura","$datosXMLinvoicesimple");
});

$('body').on('click', '#enviar_por_correo', function(b){
	b.preventDefault();
	valor_correo=$('#correo_electronico').val();
	console.log("el correo electrónico a usar es "+valor_correo);

	$.ajax({
            type: "POST",
            url: "enviar_correo.php",
            data: "accion=enviar&correo_electronico="+valor_correo+"&documento_tipo=$tipo_documento_xx&documento_serie=$numero_factura_xx&documento_monto=$monto_total_xx", // serializes the form's elements.
            beforeSend: function(data)
            {
             
                $("#mensajes_correo").fadeIn().text("Enviando ...");

            },
            success: function(data)
            {
                //alert(data); // show response from the php script.
                $("#mensajes_correo").fadeIn().text("Correo enviado").delay(1500).fadeOut();
                //$(".av_oculto").fadeIn().delay(1500).fadeOut();

            }
        });




	/* INICIO ENVIO CORREO ELECTRÓNICO */

	/* FIN ENVIO CORREO ELECTRÓNICO */
	
});

});
</script>
EOF;
		return $resultado;
	}



/* GENERAR PDF */	
public function generar_pdf(){
		global $espacio_nombres;
		global $correo_servicio_activo;
		
		
		foreach($this->monto["impuesto"] as $tipo_impuesto){
			if(floatval($tipo_impuesto["monto"])!=0){
//				$impuestos_texto.='<div class="col-md-6 bold">'.$tipo_impuesto["tipo"].'</div><div class="col-md-6 bold text-right">'.$tipo_impuesto["monto"].' </div>';}
		$ximpuestos_texto.='
<table style="width:100%;">
	<tr style="width:100%"><td style="width:50%;font-weight:bold;">'.$tipo_impuesto["tipo"].'</td><td class="bold" style="width:50%;text-align:right;" align="right">'.$tipo_impuesto["monto"].'</td></tr>
	<tr style="width:100%;"><td style="width:50%;">Operación Gratuita</td><td class="bold text-right" style="width:50%;text-align:right;" align="right">'.$this->monto["monto_operaciones_gratuitas"].'</td></tr>
</table>	
';	

		$impuestos_texto='
	<tr style="width:100%"><td style="width:50%;font-weight:bold;">'.$tipo_impuesto["tipo"].'</td><td class="bold" style="width:50%;text-align:right;" align="right">'.$tipo_impuesto["monto"].'</td></tr>

';		

		}
}
		
        $this->monto["monto_subtotal"]=
							floatval($this->monto["monto_operaciones_gravadas"])+
							floatval($this->monto["monto_operaciones_exportacion"])+
							floatval($this->monto["monto_operaciones_gratuitas"])+
							floatval($this->monto["monto_operaciones_exoneradas"])+
							floatval($this->monto["monto_operaciones_inafecto"]);
							$this->monto["monto_subtotal"]=number_format((float)$this->monto["monto_subtotal"], 2, '.', '');
        foreach($this->articulos as $articulo){
        	$precio_unitario=number_format((float)$articulo["articulo_precio_unitario"], 2, '.', '');
        	$articulo_texto.='<tr style="width:100%;"><td class="col-md-1" style="width:8.6%;">'.$articulo["articulo_cantidad"].'</td>
          <td class="col-md-1" style="width:8.6%;">'.$articulo["articulo_unidad_medida"].'</td>
          <td class="col-md-5" style="width:41%;">'.$articulo["articulo_descripcion"].'</td>
          <td class="col-md-1" style="width:8.6%;"></td>
          <td class="col-md-2" style="width:12%;">'.$precio_unitario.'</td>
          <td class="col-md-2" style="width:12%;">'.$articulo["articulo_subtotal"].'</td></tr>';

        }
/*if(floatval($this->monto["total_descuentos"])!=0){
	$descuentos.='
<table style="width:100%;">
	<tr style="width:100%"><td style="width:50%">Total Descuentos</td><td class="bold text-right" style="width:50%;text-align:right;" align="right">'.$this->monto["total_descuentos"].'</td></tr>
</table>	
';
}
if(floatval($this->monto["monto_descuento_global"])!=0){
 $descuentos.='
<table style="width:100%;">
	<tr style="width:100%"><td style="width:50%">Descuentos Globales</td><td class="bold text-right" style="width:50%;text-align:right;" align="right">'.$this->monto["monto_descuento_global"].'</td></tr>
</table>	';
        }
*/
        if(floatval($this->monto["total_descuentos"])!=0){
	$descuentos='
<table style="width:100%;">
	<tr style="width:100%"><td style="width:50%">Total Descuentos</td><td class="bold text-right" style="width:50%;text-align:right;" align="right">'.$this->monto["total_descuentos"].'</td></tr>
</table>	
';
}
if(floatval($this->monto["monto_descuento_global"])!=0){
 $descuentos='
<table style="width:100%;">
	<tr style="width:100%"><td style="width:50%">Descuentos Globales</td><td class="bold text-right" style="width:50%;text-align:right;" align="right">'.$this->monto["monto_descuento_global"].'</td></tr>
</table>	';
        }

$porciones = explode("-", $this->datos_factura["numero_factura"]);
$resultado=$this->generar_qr($this->datos_factura["emisor_ruc"],$this->datos_factura["tipo_documento"],$porciones[0],$porciones[1], $this->monto["monto_operaciones_igv"], $this->monto["monto_total"], $this->datos_factura["fecha_emision"], $this->datos_factura["cliente_tipo_documento_cod"], $this->datos_factura["cliente_ruc"]);
        //$resultado=$this->generar_qr("20374656741","01","F001","00000007","19.50","141.50","2018-08-02","01","10481484524");
        $imagen_qr='<img src="'.$resultado.'">';

		$resultado='
<style>

.borde-redondeado{
  border-radius: 10px;
  border:1px #000 solid;
}
.emisor-titulo{
  font-size: 1.3em;
  font-weight: bold;
  color:#f21379;
}
.emisor-ruc, .emisor-numerodoc{
  font-size: 1em;
  margin-top:15px;
}
.emisor-tipodocumento{
   font-size: 1.3em;
   font-weight: bold;
   margin-top:15px;
}
.adquiriente{
  margin-top: 10px;
}
.adquiriente-titulo{
   font-size: 1.3em;
   font-weight: bold;
}
.bold{
  font-weight: bold;
}
.sup-15{
  margin-top: 15px;
}
.encabezado{
  background-color: grey;
  color:#fff;
}
.articulo{
  border-left: 1px dotted #fff;
  border-right: 1px dotted #fff;
  border-bottom: 1px dotted #fff;
}
.importe_total{
   font-size: 1.1em;
  font-weight: bold;
  color:#f21379;
}
.jumbotron {
    padding: 2rem 1rem;
    margin-bottom: 2rem;
    background-color: #e9ecef;
    border-radius: .3rem;
}
.text-center{
	text-align:center;
}
</style>
<table style="width: 100%;">
<tr style="width: 100%; clear:both; ">
	<td style="width: 15.333333%;  vertical-align: text-top;" ><img src="imagenes/logo_classic.png" style="width:180px;"></td>
	<td style="width: 45.333333%;  padding-left:10%; "><div class="emisor-titulo">'.$this->datos_factura["emisor"].'</div><br>
	<div class="emisor-direccion1">'.$this->datos_factura["emisor_direccion"].'</div><br>
	<div class="emisor-direccion2">'.$this->datos_factura["emisor_distrito"].' - '.$this->datos_factura["emisor_provincia"].' - '.$this->datos_factura["emisor_departamento"].'</div></td>
	<td style="width:35%; "> 
          <div class="col-md-5 col-xs-12  borde-redondeado" style="width: 100%;  ">
            <div class="row"><div class="col-md-12 emisor-ruc text-center">RUC : '.$this->datos_factura["emisor_ruc"].'</div></div>
            <div class="row"><div class="col-md-12 emisor-tipodocumento text-center">'.strtoupper($this->tipo_documento[$this->datos_factura["tipo_documento"]]." ELECTRÓNICA").'</div></div>
            <div class="row"><div class="col-md-12 emisor-numerodoc text-center">Nº : '.$this->datos_factura["numero_factura"].'</div></div>

          </div>


	</td>
</tr>
</table>
<table style="width: 100%;height:70px;">
<tr style="width=100%; clear:both;height:70px; ">
	<td style="width:70%; height:70px;">
		       <div class="row"><div class="col-md-12 adquiriente-adquiriente">Adquiriente</div></div>
              <div class="row"><div class="col-md-12 adquiriente-titulo">'.$this->datos_factura["cliente_nombre"].'</div></div>
              <div class="row"><div class="col-md-12 emisor-direccion1">'.$this->tipo_doc_identidad[$this->datos_factura["cliente_tipo_documento_cod"]].' : '.$this->datos_factura["cliente_ruc"].'</div></div>
              <div class="row"><div class="col-md-12 emisor-direccion2">'.$this->datos_factura["cliente_direccion"].'</div></div>

	</td>
	<td style="width:30%;height:70px; position:relative;">

	<div class="row" style="display:block;clear:both;">
              <div class="col-md-6" style="width:50%;  float:left;">Fecha Vencimiento</div><div class="col-md-6" style=" width:50%;float:left;">Moneda</div>
            </div>
<div class="row" style="display:block;clear:both;">

<div class="col-md-6 bold" style="width:50%; float:left;">'.$this->datos_factura["fecha_emision"].'</div><div class="col-md-6 bold" style="width:50%;  float:left;">'.$this->tipo_moneda[$this->datos_factura["moneda"]].'</div>
</div>            


	<div class="row" style="display:block;clear:both;">
              <div class="col-md-6" style="width:50%;  float:left;">Fecha Emisión</div><div class="col-md-6" style=" width:50%;float:left;">IGV</div>
            </div>
<div class="row" style="display:block;clear:both;">

<div class="col-md-6 bold" style="width:50%; float:left;">'.$this->datos_factura["fecha_emision"].'</div><div class="col-md-6 bold" style="width:50%;  float:left;">18%</div>
</div>            


	</td>
</tr>
</table>

<table style="width: 100%; margin-top:40px;" >
<tr style="width: 100%;" class="encabezado">
          <td class="col-md-1" style="width:8.6%;">Cant:</td>
          <td class="col-md-1" style="width:8.6%;">Unidad</td>
          <td class="col-md-5" style="width:41%;">Desc</td>
          <td class="col-md-1" style="width:8.6%;">Descuento</td>
          <td class="col-md-2" style="width:12%;">Precio Unitario</td>
          <td class="col-md-2" style="width:12%;">Importe Total</td>
</tr>
'.$articulo_texto.'
</table>
<table style="width:100%;" >
<tr style="width:100%">
	<td style="width:58%">Resumen : <strong>'.$this->datos_factura["digestvalue"].'<br>
	'.$imagen_qr.'
	</strong>

	</td>
	<td style="width:42%"> 
	<table style="width:100%;">
	<tr style="width:100%"><td style="width:50%">Operación Gravada</td><td class="bold text-right" style="width:50%;text-align:right;" align="right">'.$this->monto["monto_operaciones_gravadas"].'</td></tr>
	<tr style="width:100%"><td style="width:50%">Operación Inafecta</td><td class="bold text-right" style="width:50%;text-align:right;" align="right">'.$this->monto["monto_operaciones_inafecto"].'</td></tr>
	<tr style="width:100%"><td style="width:50%">Operación Exonerada</td><td class="bold text-right" style="width:50%;text-align:right;" align="right">'.$this->monto["monto_operaciones_exoneradas"].'</td></tr>
	<tr style="width:100%"><td style="width:50%">Operación Gratuita</td><td class="bold text-right" style="width:50%;text-align:right;" align="right">'.$this->monto["monto_operaciones_gratuitas"].'</td></tr>
	'.$descuentos.' '.$impuestos_texto.'
	
	<tr style="width:100%"><td style="width:50%">Sub Total</td><td class="bold text-right" style="width:50%;text-align:right;" align="right">'.$this->monto["monto_subtotal"].'</td></tr>
	<tr style="width:100%"><td style="width:50%" class="importe_total">Importe Total</td><td class="bold text-right importe_total" style="width:50%:text-align:right;" align="right">'.$this->monto["monto_total"].'</td></tr>
	</table>
	</td>
</tr>	
</table>


		
      ';
      $otracosa='<div class="row text-center">
    
      </div>
    
      <div class="jumbotron" style="margin-top40px;">
             

        <div class="row adquiriente">
   
          <div class="col-md-5 col-xs-12 offset-md-2">
            <div class="row">
              <div class="col-md-6">Operación Gravada</div><div class="col-md-6 text-right">'.$this->monto["monto_operaciones_gravadas"].'</div>
              <div class="col-md-6">Operación Inafecta</div><div class="col-md-6 bold text-right">'.$this->monto["monto_operaciones_inafecto"].'</div>
              <div class="col-md-6">Operación Exonerada</div><div class="col-md-6 bold text-right">'.$this->monto["monto_operaciones_exoneradas"].'</div>
              <div class="col-md-6">Operación Gratuita</div><div class="col-md-6 bold text-right">'.$this->monto["monto_operaciones_gratuitas"].'</div>
            </div>

            <div class="row sup-15">
            '.$xdescuentos.'
            
              <div class="col-md-6">Sub Total</div><div class="col-md-6 text-right">'.$this->monto["monto_subtotal"].'</div>
              
              <!-- div class="col-md-6 bold">IGV</div><div class="col-md-6 bold text-right">18%</div -->
            </div>

            <div class="row">
              
            </div>

            <div class="row">
              <div class="col-md-6 importe_total">Importe Total</div><div class="col-md-6 text-right importe_total">'.$this->monto["monto_total"].'</div>
              
            </div>
            

          </div>
        </div>



      </div>';
      $ee='';
      $datosXMLinvoicesimple=addslashes($this->datosXMLinvoice);
      $datosXMLinvoicesimple = str_replace(array("\r\n", "\n", "\r"), ' ', $datosXMLinvoicesimple);
      $nombre_factura=$this->datos_factura["numero_factura"].".xml";
      $tipo_documento_xx=$this->datos_factura["tipo_documento"];
      $numero_factura_xx=$this->datos_factura["numero_factura"];
      $monto_total_xx=$this->monto["monto_total"];

		return $resultado;
	}

/* FIN GENERAR PDF */

	public function mostrar_busqueda($mensaje=""){
		global $espacio_nombres;
		$resultado=' <div class="row text-center">
      <img src="imagenes/logo_classic.png" class="img-responsive img-fluid center-block" style="margin:auto;">
      </div>
      <div class="jumbotron">
        <form method="post" action="ver.php">
        <h3>Consulta Facturación Electrónica</h3>
        <h4>'.$mensaje.'</h4>
        
        <div class="row">
          <div class="col-md-6 col-xs-12">Tipo Documento:</div>
          <div class="col-md-6 col-xs-12">
            <select name="documento_tipo" class="form-control">
              <option disabled>Seleccione tipo de documento</option>
              <option value="01">Factura</option>
              <option value="02">Boleta de venta</option>
              <option value="03">Nota de Crédito</option>
              <option value="04">Nota de Debito</option>
              <option value="04">Guia de remisión remitente</option>
              <option value="04">Ticket de maquina registradora</option>
              <option value="04">Doc. emitido por Bancos</option>
              <option value="04">Recibo Servicios Publicos</option>
              <option value="04">Documentos emitidos por las AFP</option>
              <option value="04">Guia de Remisión Transportista</option>
              <option value="04">Comprobante de pago SEAE</option>
              <option value="04">Guia de Remisión remitente Complementaria</option>
              <option value="04">Guia de Remisión transportista complementaria</option>
            </select>
         
          </div>
        </div>

        <div class="row">
          <div class="col-md-6 col-xs-12">Folio (Serie-Numero):</div><div class="col-md-6 col-xs-12"><input type="text" name="documento_serie" class="form-control" placeholder="F12-111100114"></div>
        </div>
        <div class="row">
          <div class="col-md-6 col-xs-12">Fecha Emisión:</div><div class="col-md-6 col-xs-12"><input type="text" name="documento_fecha" class="form-control" placeholder="2018-08-22"></div>
        </div>
        <div class="row">
          <div class="col-md-6 col-xs-12">Monto:</div><div class="col-md-6 col-xs-12"><input type="text" name="documento_monto" class="form-control" placeholder="212.11"></div>
        </div>


        <div class="row">
          <div class="col-md-6 col-xs-12"></div><div class="col-md-6 col-xs-12">
          <input type="submit" name="enviar" class="btn btn-lg btn-primary" value="Consultar &raquo;">
        </div>
        </div>

        
      
      </form>
      </div>';
      return $resultado;



	}
}
?>