<?php
class Plantilla{
    public $tipo_plantilla;
    public $buscar;
    public $reemplazar;

    private $plantilla;
    private $TEMPLATE;
    private $platilla_interna;



    private $init_crear_cliente  = array('%%nombre%%', '%%apellido%%', '%%email%%', '%%fecha_nacimiento%%', '%%telefono%%', '%%telefono2%%', '%%pasaporte_numero%%');
    private $init_crear_cliente_reemplazo = array('', '', '', '', '', '', '');

    public function set_Plantilla($plantilla) { 
        $this->plantilla = $plantilla; 
    }
    public function get_Plantilla() { 
        return $this->plantilla; 
    }
public function obtener_plantilla($plantilla){
    $valor=implode("", file($plantilla));
    return $valor;
}

public function obtener_plantilla_interna($plantilla){
    $this->plantilla_interna=implode("", file($plantilla));
}

public function init_crear_cliente(){
    global $cliente;
    $this->plantilla_interna=str_replace($this->init_crear_cliente, $this->init_crear_cliente_reemplazo, $this->plantilla_interna);
    $this->plantilla_interna = str_replace("%%PAISES_PASAPORTE%%",Paises::listarpaises(230,"pasaporte_pais","pasaporte_pais"),$this->plantilla_interna);

    $this->plantilla_interna = str_replace("%%nacionalidad%%",Paises::listarpaises(230,"nacionalidad","nacionalidad"),$this->plantilla_interna);
    $this->plantilla_interna = str_replace("%%paisresidencia%%",Paises::listarpaises(230,"paisresidencia","paisresidencia"),$this->plantilla_interna);

    $this->plantilla_interna = str_replace("%%TARJETAPAISES%%",Paises::listarpaises(230,"tarjeta_pais","tarjeta_pais","registrar_tarjeta",1),$this->plantilla_interna); 
    $this->plantilla_interna = str_replace("%%TARJETAESTADOS%%",Paises::listarestadospaises("US",0,"tarjeta_estado","tarjeta_estado","registrar_tarjeta",1),$this->plantilla_interna); 

    $this->plantilla_interna = str_replace("%%PAISES%%",Paises::listarpaises(230),$this->plantilla_interna); 
    $this->plantilla_interna = str_replace("%%ESTADOS%%",Paises::listarestadospaises("US"),$this->plantilla_interna); 
    $this->plantilla_interna = str_replace("%%genero%%",$cliente->select_genero(),$this->plantilla_interna);
}


public function init_crear_proveedor(){
    global $proveedores;
    global $Idiomas;
    
    $this->plantilla_interna = str_replace("%%PAISES_PASAPORTE%%",Paises::listarpaises(230,"pasaporte_pais","pasaporte_pais"),$this->plantilla_interna);
    $this->plantilla_interna = str_replace("%%paises%%",Paises::listarpaises(230),$this->plantilla_interna); 
    $this->plantilla_interna = str_replace("%%estados%%",Paises::listarestadospaises("US",0,"estado","estado"),$this->plantilla_interna); 

    $this->plantilla_interna = str_replace("%%paises_facturacion%%",Paises::listarpaises(230,"facturacion_pais","facturacion_pais"),$this->plantilla_interna); 
    $this->plantilla_interna = str_replace("%%estados_facturacion%%",Paises::listarestadospaises("US",0,"facturacion_estado","facturacion_estado"),$this->plantilla_interna); 


    $this->plantilla_interna = str_replace("%%metodo_pago%%",$proveedores->select_metodo_pago(),$this->plantilla_interna);
    $this->plantilla_interna = str_replace("%%tipo_contrato%%",$proveedores->select_tipo_contrato(),$this->plantilla_interna);
    $this->plantilla_interna = str_replace("%%contacto_cargo%%",$proveedores->select_contacto_cargo(),$this->plantilla_interna);
    $this->plantilla_interna = str_replace("%%categoria%%",$proveedores->select_categoria(),$this->plantilla_interna);


    

    $this->plantilla_interna = str_replace("%%idioma%%",$proveedores->select_gen($Idiomas->idiomalista,0,0,0,"idioma","idioma"),$this->plantilla_interna);

}

public function init_crear_rentadora(){
    
    global $Idiomas;
    
    $this->plantilla_interna = str_replace("%%PAISES_PASAPORTE%%",Paises::listarpaises(230,"pasaporte_pais","pasaporte_pais"),$this->plantilla_interna);
    $this->plantilla_interna = str_replace("%%paises%%",Paises::listarpaises(230),$this->plantilla_interna); 
    $this->plantilla_interna = str_replace("%%estados%%",Paises::listarestadospaises("US",0,"estado","estado"),$this->plantilla_interna); 

    $this->plantilla_interna = str_replace("%%paises_facturacion%%",Paises::listarpaises(230,"facturacion_pais","facturacion_pais"),$this->plantilla_interna); 
    $this->plantilla_interna = str_replace("%%estados_facturacion%%",Paises::listarestadospaises("US",0,"facturacion_estado","facturacion_estado"),$this->plantilla_interna); 


}

public function init_crear_agencia(){
    global $agencias;
    global $Idiomas;
    
    $this->plantilla_interna = str_replace("%%PAISES_PASAPORTE%%",Paises::listarpaises(230,"pasaporte_pais","pasaporte_pais"),$this->plantilla_interna);
    $this->plantilla_interna = str_replace("%%paises%%",Paises::listarpaises(230),$this->plantilla_interna); 
    $this->plantilla_interna = str_replace("%%estados%%",Paises::listarestadospaises("US",0,"estado","estado"),$this->plantilla_interna); 

    $this->plantilla_interna = str_replace("%%paises_facturacion%%",Paises::listarpaises(230,"facturacion_pais","facturacion_pais"),$this->plantilla_interna); 
    $this->plantilla_interna = str_replace("%%estados_facturacion%%",Paises::listarestadospaises("US",0,"facturacion_estado","facturacion_estado"),$this->plantilla_interna); 


    $this->plantilla_interna = str_replace("%%metodo_pago%%",$agencias->select_metodo_pago(),$this->plantilla_interna);
    $this->plantilla_interna = str_replace("%%tipo_contrato%%",$agencias->select_tipo_contrato(),$this->plantilla_interna);
    $this->plantilla_interna = str_replace("%%contacto_cargo%%",$agencias->select_contacto_cargo(),$this->plantilla_interna);
    $this->plantilla_interna = str_replace("%%categoria%%",$agencias->select_categoria(),$this->plantilla_interna);


    

    $this->plantilla_interna = str_replace("%%idioma%%",$agencias->select_gen($Idiomas->idiomalista,0,0,0,"idioma","idioma"),$this->plantilla_interna);

}


public function reemplazar_valores_plantilla_interna($tipo=0){
    global $cliente;
    $clave = array_search('%%pasaporte_pais%%', $this->buscar);
    $genero_clave = array_search('%%genero%%', $this->buscar);

    $clave_pais = array_search('%%pais%%', $this->buscar);
    $clave_estado = array_search('%%estado%%', $this->buscar);
    $this->plantilla_interna = str_replace("%%PAISES_PASAPORTE%%",Paises::listarpaises($this->reemplazar[$clave],"pasaporte_pais","pasaporte_pais"),$this->plantilla_interna);
    //echo "aaaaaaaaaaaaaaaaaaaqq ".print_r($this->reemplazar,1)."<br>";    
    //echo "aaaaaaaaaaaaaaaaaaa ".$this->reemplazar["paises"]."<br>";
    $this->plantilla_interna = str_replace("%%pais_nombre%%",Paises::obtenernombrepais($this->reemplazar[$clave_pais]),$this->plantilla_interna);
    $this->plantilla_interna = str_replace("%%estado_nombre%%",Paises::obtenernombreestado($this->reemplazar[$clave_estado],$this->reemplazar[$clave_pais]),$this->plantilla_interna);

    $this->plantilla_interna = str_replace("%%genero%%",$cliente->select_genero($this->reemplazar[$genero_clave]),$this->plantilla_interna);
    
    $this->plantilla_interna=str_replace($this->buscar, $this->reemplazar, $this->plantilla_interna);
    //$this->plantilla_interna=$this->plantilla_interna;

preg_match_all("/\{\('([a-zA-Z 0-9]+)'\)\}/", $this->plantilla_interna,  $salida, PREG_PATTERN_ORDER);
//    preg_match_all("{('[a-zA-Z 0-9]+')}", $this->plantilla_interna,  $salida, PREG_PATTERN_ORDER);
print_r($salida);
    $buscar_traduccion[]="";
    $reemplazar_traduccion[]="";

$Idiomas=new Idiomas();    
/*
foreach ($salida as $key => $value)
{
  if (is_array($value))
 {
  foreach ($value as $ikey => $ivalue)
  {
    $buscar_traduccion[$ikey]="{".$ivalue."}";
    $reemplazar_traduccion[$ikey]=$Idiomas->traduccion($ivalue);
    //echo $ikey . ' => ' . $ivalue . '<br />';
  }
 }  
}
*/



  foreach ($salida[1] as $ikey => $ivalue)
  {
    $buscar_traduccion[$ikey]="{('".$ivalue."')}";
    $reemplazar_traduccion[$ikey]=$Idiomas->traduccion($ivalue);
    //echo $ikey . ' => ' . $ivalue . '<br />';
  }
 
print_r($buscar_traduccion);
print_r($reemplazar_traduccion);

$this->plantilla_interna=str_replace($buscar_traduccion, $reemplazar_traduccion, $this->plantilla_interna);
   // public function generar_matriz_reemplazo($row){
/* foreach(array_keys($row) as $key )
{
    $this->buscar[]="%%".$key."%%";
    $this->reemplazar[]=$row[$key];
} */

    




}
public function devolver_plantilla_interna(){
    return $this->plantilla_interna;
}

public function ocultar_secciones($inicio,$fin){
   // Inicio::Ocultar::Edicion
    $this->plantilla_interna=str_replace("<!--".$inicio."-->","<!--",$this->plantilla_interna);
    $this->plantilla_interna=str_replace("<!--".$fin."-->","-->",$this->plantilla_interna);

   //$search = "/[^<!--".$inicio."-->](.*)[^<!--".$fin."-->]/";
  /*  $search = "/[^<!--".$inicio."-->](.*)[^<!--".$fin."-->]/";
    $search =preg_quote($search);
    $replace = "";
    preg_replace($search,$replace,$this->plantilla); */
}

public function reemplazar_valores_obtener_plantilla(){
    global $codigo;
$codigo = str_replace("%%PAISES_PASAPORTE%%",Paises::listarpaises(230,"pasaporte_pais","pasaporte_pais"),$codigo);
$codigo = str_replace("%%PAISES%%",Paises::listarpaises(230),$codigo); 
$codigo = str_replace("%%ESTADOS%%",Paises::listarestadospaises("US"),$codigo); 

}

public function generar_matriz_reemplazo($row){
foreach(array_keys($row) as $key )
{
    $this->buscar[]="%%".$key."%%";
    $this->reemplazar[]=$row[$key];
}
/*echo "Generar Matriz de reemplazo<br>";
print_r($this->buscar);
print_r($this->reemplazar); */
}
public function reemplazar_valores_plantilla($minicartx=1){

    //$TEMPLATE2=$this->plantilla;
    $TEMPLATE2=implode("", file($this->plantilla));
    global $idiomas,$paraflash,$seleccionados,$favoritos,$accesosalir,$ordenar,$minicart,$nombre,$codigo,$paginas,$mensaje,$principal_inferior,$url_script_imagen_principal;
// echo "LOCA xxxx".$minicartx; facebook
//$accesosalir=accesosalir();
/*MINICART_CARRITODECOMPRAS */
$javascript='
<script language="JavaScript"> 
$(document).ready(function(){
        $("#minicarritoicono").hide();                 
                            });
</script>';
$enviogratis='<div class="enviogratis_titulo">'.ENVIOGRATIS_TITULO.'</div><div class="enviogratis_descripcion">'.ENVIOGRATIS_DESC.'</div>';
//$subtotal=totales_simple();
//$valor=totales_simple();
//$idiomas=listar_idiomas();
$subtotal= "<a href='carrito_compras.php'>Total ".$valor." ".IR_A_CARRITO.": </a>";
//$bloquecategorias=bloquecategorias();
$TEMPLATE2 = str_replace("%%BLOQUEPRODUCTOS%%",$bloquecategorias,$TEMPLATE2);
$TEMPLATE2 = str_replace("%%IFP%%",IDIOMA_FACEBOOK_PLUGINS,$TEMPLATE2); 


$TEMPLATE2 = str_replace("%%CONTACTENOS%%",GENERAL_CONTACTENOS,$TEMPLATE2); 
$TEMPLATE2 = str_replace("%%REGISTRESE%%",GENERAL_REGISTRSE,$TEMPLATE2);    
$TEMPLATE2 = str_replace("%%INGRESE%%",GENERAL_INGRESE,$TEMPLATE2); 

$TEMPLATE2 = str_replace("%%BUSCAR%%",GENERAL_BUSCAR,$TEMPLATE2);   
$TEMPLATE2 = str_replace("%%DESTINOS_DESTACADOS%%",GENERAL_DESTINOS_DESTACADOS,$TEMPLATE2); 
$TEMPLATE2 = str_replace("%%ENCUENTRE_MAS_DESTINOS%%",GENERAL_ENCUENTRE_MAS_DESTINOS,$TEMPLATE2);   
$TEMPLATE2 = str_replace("%%MAS_INFORMACION%%",GENERAL_MAS_INFORMACION,$TEMPLATE2); 
$TEMPLATE2 = str_replace("%%IMAGEN%%",GENERAL_IMAGEN,$TEMPLATE2);   
$TEMPLATE2 = str_replace("%%QUIENES_SOMOS%%",GENERAL_QUIENES_SOMOS,$TEMPLATE2); 
$TEMPLATE2 = str_replace("%%PAQUETES_TURISTICOS%%",GENERAL_PAQUETES_TURISTICOS,$TEMPLATE2); 
$TEMPLATE2 = str_replace("%%CALENDARIO_TURISTICO%%",GENERAL_CALENDARIO_TURISTICO,$TEMPLATE2);   

//$TEMPLATE2 = str_replace("%%DIRECCION%%",configuracion_valores("direccion"),$TEMPLATE2);    




$TEMPLATE2 = str_replace("%%recommendus%%",WM_RECOMENDAR,$TEMPLATE2);               
$TEMPLATE2 = str_replace("%%welcomemembers%%",WM_BIENVENIDOS,$TEMPLATE2);               
$TEMPLATE2 = str_replace("%%addtofavorites%%",WM_FAVORITOS,$TEMPLATE2);             


$TEMPLATE2 = str_replace("%%ENVIOGRATIS%%",$enviogratis ,$TEMPLATE2);               
$TEMPLATE2 = str_replace("%%MINICARRITORESUMEN%%",$subtotal ,$TEMPLATE2);               
$TEMPLATE2 = str_replace("%%CARRITOTITULO%%",MINICART_CARRITODECOMPRAS  ,$TEMPLATE2);               
$TEMPLATE2 = str_replace("%%IDIOMAS%%",$idiomas,$TEMPLATE2);                
$TEMPLATE2 = str_replace("%%FLASH%%",$paraflash,$TEMPLATE2);
$TEMPLATE2 = str_replace("%%SELECCIONADOS%%",$seleccionados,$TEMPLATE2);
$TEMPLATE2 = str_replace("%%FAVORITOS%%",$favoritos,$TEMPLATE2);
$TEMPLATE2 = str_replace("%%ACCESO%%",$accesosalir,$TEMPLATE2);
$TEMPLATE2 = str_replace("%%ORDENAR%%",$ordenar,$TEMPLATE2);
if($minicartx==1){
    //echo "LOCA";
$TEMPLATE2 = str_replace("%%MINICART%%",$minicart,$TEMPLATE2);  
}else{
    $TEMPLATE2 = str_replace("%%MINICART%%",$javascript,$TEMPLATE2);    
    }   
$TEMPLATE2 = str_replace("%%NOMBRE%%",$nombre,$TEMPLATE2);  
$TEMPLATE2 = str_replace("%%PRINCIPAL_INFERIOR%%",$principal_inferior,$TEMPLATE2);
$TEMPLATE2 = str_replace("%%PRODUCTOS%%",$codigo,$TEMPLATE2);
$TEMPLATE2 = str_replace("%%CONTENIDO%%",$codigo,$TEMPLATE2);
$TEMPLATE2 = str_replace("%%PAGINAS%%",$paginas,$TEMPLATE2);
$TEMPLATE2 = str_replace("%%MENSAJES%%",$mensaje,$TEMPLATE2);

$TEMPLATE2 = str_replace("%%URL_IMAGEN_PRINCIPAL%%",$url_script_imagen_principal[0],$TEMPLATE2);
$TEMPLATE2 = str_replace("%%SCRIPT_IMAGEN_PRINCIPAL%%",$url_script_imagen_principal[1],$TEMPLATE2);
$this->TEMPLATE=$TEMPLATE2;
}

public function mostrar_resultado_plantilla(){
   // global $TEMPLATE2;
    echo $this->TEMPLATE;
}


public function formulariosauthadmin($tipo=0,$mensaje=""){
    $cod2="";
    $destino="";
    if($tipo==1){
        $cod="<div class='datos rojo'>".$mensaje."</div>";
        }
    if($tipo==2){
        $cod2="<div class='datos rojo'>".$mensaje."</div>";
        }   
    if(isset($_REQUEST["destino"])){
                       $destino=' <input type="hidden" name="destino" value="'.$_REQUEST["destino"].'" />';

/* configuracion<script language="javascript" type="text/javascript"  src="themes/primero/js/niceforms.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="themes/primero/css/niceforms.css" / >
*/                     }
/* No Se Encontraron Productos en Su Carrito micuentadetallesadmin*/
    $html='
<div class="producto_cabezera_final"><div class="nombrecate">AUTENTIFICAR</div></div>
<div class="productoscuerpo">
  <div class="registrarse">Ingrese Su Correo Electronico y Password para acceder al Panel de Administracion</div>
  <div class="ingresar">
    <form method="post" action="autentificar.php" class="niceform">
    <input type="hidden" name="acceso" value="1" />
    <div class="titulo">Ingrese</div>'.$cod2.$destino.'
    <div class="datos"><div class="texto">Usuario</div><div class="valor"><input type="text" size="40" name="usuario"/>  </div></div>
    <div class="datos"><div class="texto">Password</div><div class="valor"><input type="password" size="40" name="password"/>  </div></div>      
    <div class="botonenviar"><input type="submit"  value="Ingresar" />  </div>
    </form>
  </div>
  </div>
  <div class="productos-inferior"></div>'.self::colocar_pie(2);
  return $html;
    }       

public static function colocar_pie($valor){

}

}
?>